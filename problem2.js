const fs = require('fs');
const path = require("path");

const lipsum = path.resolve(__dirname, "./lipsum.txt");
const filesnames = path.resolve(__dirname, "./filesnames.txt");

const readFilePro = file => {
  return new Promise((resolve, reject) => {
    fs.readFile(file, 'utf-8', (err, data) => {
      if (err) reject('could not find that file 😢');
      resolve(data);
      console.log(`reading ${file} file`);
    });
  });
};

const writeFilePro = (file, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(file, data, err => {
      if (err) reject('Could not write file 😢');
      resolve('success');
    });
  });
};

const appendfile = (file, data) => {
  return new Promise((resolve, reject) => {
    fs.appendFile(file, data, err => {
      if (err) reject('Could not append file 😢');
      resolve('success');
    });
  });
}

const unlinkfile = (file) => {
  return new Promise((resolve, reject) => {
    fs.unlink(file, err => {
    //  if (err) reject('Could not delete file 😢');
      resolve('success');
    });
  });
}


readFilePro(lipsum).then(data => {
  let UpperCaseData = data.toString().toUpperCase()
  return UpperCaseData
}).then(uppercasedata => {
  writeFilePro('uppercase.txt', uppercasedata)
  appendfile('filesnames.txt', 'uppercase.txt' + '\n');
  return uppercasedata
}).then(uppercasedata => {
  let LowercaseData = uppercasedata.toString().toLowerCase().split(".").join(".\n");
  return LowercaseData
}).then(lowercasedata => {
  writeFilePro('lowercase.txt', lowercasedata)
  appendfile('filesnames.txt', 'lowercase.txt' + '\n');
  return lowercasedata
}).then(lowercasedata => {
  let soretedData = lowercasedata.toString().split(".").sort().join(".\n");
  // console.log(soretedData);
  return soretedData;
}).then(soreteddata => {
  writeFilePro('soretedData.txt', soreteddata);
  appendfile('filesnames.txt', 'soretedData.txt' + '\n');
}).then(() => {
  console.log("data converted");
}).catch(err => {
  console.log(err);
})


readFilePro(filesnames)
  .then(data =>{
     let file =[];
     newData = (data.split('\n').toString())
    file.push(newData.split(','))
   return file[0];
  }).then(filenames =>{
    filenames.forEach(element => {
      console.log(element);
      unlinkfile(element)
    });
  }).then(()=>{
    writeFilePro(filesnames, '');
  }).catch(err => {
    console.log("cant't delete file");
  })
  